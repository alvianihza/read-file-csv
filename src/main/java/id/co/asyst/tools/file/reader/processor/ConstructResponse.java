package id.co.asyst.tools.file.reader.processor;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConstructResponse {

    private static Logger logger = LogManager.getLogger(ConstructResponse.class);

    public void errorResponse(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Exception except = exchange.getException();
        String body = in.getBody(String.class);
        Integer httpResponseCode = in.getHeader(exchange.HTTP_RESPONSE_CODE, Integer.class);
        Gson gson = new Gson();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
//        if (identity.containsKey("password")) {
//            identity.replace("password",
//                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
//        }

        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Date date = new Date();
//        identity.replace("reqdate", sdf2.format(date));
        status.put("responsecode", "9999");
        status.put("responsedesc", "error");
        status.put("responsemessage", exchange.getProperty("resmsg"));
//        status.put("responsemessage", in.getHeader("error", String.class));

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        String result = gson.toJson(getServiceResponse);
        logger.info("result error" + result);
        out.setBody(result);

    }

    public void successResponse(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Exception except = exchange.getException();
        String body = in.getBody(String.class);
        Integer httpResponseCode = in.getHeader(exchange.HTTP_RESPONSE_CODE, Integer.class);
        Gson gson = new Gson();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        if (identity.containsKey("password")) {
            identity.replace("password",
                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
        }

        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Date date = new Date();
        identity.replace("reqdate", sdf2.format(date));
        status.put("responsecode", "0000");
        status.put("responsedesc", "success");
        status.put("responsemessage", "Data is being processed with id " +exchange.getProperty("id"));

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        String result = gson.toJson(getServiceResponse);
        logger.info("result" + result);
        out.setBody(result);

    }

    public void noData(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        String body = in.getBody(String.class);
        Gson gson = new Gson();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        if (identity.containsKey("password")) {
            identity.replace("password",
                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
        }

        List<Map<String, Object>> mapBodys = (List) gson.fromJson(body, List.class);
        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> parameter = new HashMap<String, Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));
        Date date = new Date();
        identity.replace("reqdate", sdf2.format(date));

        status.put("responsecode", "9999");
        status.put("responsedesc", "error");
        status.put("responsemessage", "Data not found!");

        logger.info("mapBody " + mapBodys);
        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        String result = gson.toJson(getServiceResponse);
        logger.info("result" + result);
        out.setBody(result);
        exchange.setProperty("resSuccess", result);


    }

    public void validation(Exchange exchange){
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        if (identity.containsKey("password")) {
            identity.replace("password",
                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
        }
        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        identity.replace("reqdate", sdf2.format(date));
        status.put("responsecode", "9005");
        status.put("responsedesc", "error");
        status.put("responsemessage", "field is required!");

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        String result = gson.toJson(getServiceResponse);
        logger.info("result validation" + result);
        out.setBody(result);


    }

    public void countResponse(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Exception except = exchange.getException();
        List<Map<String, Object>> body = in.getBody(List.class);
        logger.info("body " + body);
//        String body = in.getBody(String.class);
        Integer httpResponseCode = in.getHeader(exchange.HTTP_RESPONSE_CODE, Integer.class);
        Gson gson = new Gson();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));
        String total = body.get(0).get("total").toString();
        logger.info("total " + total);

        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        if (identity.containsKey("password")) {
            identity.replace("password",
                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
        }

        Map<String, Object> getServiceResponse = new HashMap<String, Object>();
        Map<String, Object> status = new HashMap<String, Object>();
        Date date = new Date();
        identity.replace("reqdate", sdf2.format(date));
        status.put("responsecode", "0000");
        status.put("responsedesc", "success");
        status.put("responsemessage", total + " " + "data is being processed with id " +exchange.getProperty("id"));

        getServiceResponse.put("identity", identity);
        getServiceResponse.put("status", status);
        String result = gson.toJson(getServiceResponse);
        logger.info("result" + result);
        out.setBody(result);

    }

}
