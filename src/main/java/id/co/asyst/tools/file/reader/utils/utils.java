package id.co.asyst.tools.file.reader.utils;

import id.co.asyst.tools.file.reader.processor.ConstructRequest;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class utils {
    private static Logger logger = LogManager.getLogger(utils.class);

    public void setIdentity (Exchange exchange) {

        LinkedHashMap<String, Object> identity = new LinkedHashMap<String, Object>();
        String apptxnid = "";
        String reqtxnid = "ea9e5895-29c9-4f98-97ef-fcb4ba96e56c";
        String reqdate = "2018-04-01 15:23:23";
        String appid = "cm-app";
        String userid = "SYSTEM";
        String password = "password1";
        String signature = "";

        identity.put( "apptxnid", apptxnid);
        identity.put( "reqtxnid", reqtxnid);
        identity.put( "reqdate", reqdate);
        identity.put( "appid", appid);
        identity.put( "userid", userid);
        identity.put( "password", password);
        identity.put( "signature", signature);

        logger.info( "Identity ::: " +identity);
        exchange.setProperty("identity", identity);


    }
}
