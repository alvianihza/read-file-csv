package id.co.asyst.tools.file.reader.utils;

import id.co.asyst.tools.file.reader.processor.ConstructResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class FileHandler {
    private static Logger logger = LogManager.getLogger(FileHandler.class);

    public void readFile (Exchange exchange){
        logger.info("readFile method");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        logger.info("Body :::" +in);
        List<String> activityid = new ArrayList<String>();
        List<List<String>> data = (List<List<String>>) exchange.getIn().getBody();
        for (List<String> line : data) {
            activityid.add(String.format(line.get(0)));
//            logger.info(activityid);
//            logger.info(String.format("%s has an IQ of %s and is currently %s", line.get(0), line.get(1), line.get(2)));
        }
        out.setBody(activityid);
    }
}
