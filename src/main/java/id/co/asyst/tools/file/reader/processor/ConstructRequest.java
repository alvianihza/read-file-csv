package id.co.asyst.tools.file.reader.processor;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

public class ConstructRequest {
    private static Logger logger = LogManager.getLogger(ConstructRequest.class);

    public void reqApproval(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Map<String, Object> body = in.getBody(Map.class);
        logger.info("body split req : " + body);
        List<String> activityid = new ArrayList<String>();
//        activityid.add(body.get("activityid").toString());
        Gson gson = new Gson();
        Message message;

        exchange.setProperty("activityid", body.get("activityid").toString());
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
//        if (identity.containsKey("password")) {
//            identity.replace("password",
//                    identity.get("password").toString().replaceAll("[A-Za-z0-9]", "*").toString());
//        }
        Map<String, Object> getServiceApprovedName = new HashMap<String, Object>();
        Map<String, Object> parameter = new HashMap<String, Object>();
        Map<String, Object> data = new HashMap<String, Object>();

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        out.setHeader("id", exchange.getProperty("idroute2a"));

        data.put("activityid", activityid);
        data.put("actiontype", "APPROVED");
        parameter.put("data", data);
        getServiceApprovedName.put("parameter", parameter);
        getServiceApprovedName.put("identity", identity);
        String result = gson.toJson(getServiceApprovedName);
        out.setBody(result);
    }

    public void reqUser(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Map<String, Object> body = in.getBody(Map.class);
        logger.info("req body user : " + body);
        Gson gson = new Gson();
        Message message;
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> getServiceApprovedName = new HashMap<String, Object>();
        Map<String, Object> parameter = exchange.getProperty("parameter", Map.class);

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));
        logger.info("id : " + exchange.getProperty("id"));
        out.setHeader("id", exchange.getProperty("id"));
        getServiceApprovedName.put("parameter", parameter);
        getServiceApprovedName.put("identity", identity);
        String result = gson.toJson(getServiceApprovedName);
        out.setBody(result);
    }

    public void tesReq (Exchange exchange){
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        String activityidin = in.getBody().toString();
        logger.info("activityid on split :::" +activityidin);
        Gson gson = new Gson();

        LinkedHashMap<String, Object> getServiceApprovedName = new LinkedHashMap<String, Object>();
        Map<String, Object> parameter = new HashMap<String, Object>();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> data = new HashMap<String, Object>();
        List<String> activityid = new ArrayList<String>();

        activityid.add(activityidin);

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        data.put("activityid", activityid);
        data.put("actiontype", "APPROVED");
        parameter.put("data", data);
        getServiceApprovedName.put("identity", identity);
        getServiceApprovedName.put("parameter", parameter);
        String result = gson.toJson(getServiceApprovedName);
        out.setBody(result);
        logger.info(result);
    }
}
